package configuration

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

// Configs return configuration settings
var ConfigRepo *ConfigRepository

type ConfigRepository struct {
	B       []byte
	Configs map[string]interface{}
}

func NewConfigRepository(configByte []byte) *ConfigRepository {
	if ConfigRepo == nil {
		if configByte == nil {
			var err error
			configByte, err = ioutil.ReadFile("./configs/config.json")
			failOnError(err, "Error on open config file.")
		}

		ConfigRepo = &ConfigRepository{B: configByte}
	}

	return ConfigRepo
}

//LoadConfig load config
func (repo *ConfigRepository) LoadConfig() error {

	repo.Configs = make(map[string]interface{})
	return json.Unmarshal(repo.B, &repo.Configs)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

//GetConfig return value of a config key
func (repo *ConfigRepository) GetConfig(key string) string {

	if repo.Configs == nil {
		repo.LoadConfig()
	}
	s := repo.Configs[key]
	return s.(string)
}
