package configuration

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func newTestConfigFile() []byte {
	config := `{"DB_HOST":"DB Host","DB_PORT":"Port of DB","DB_NAME":"Database Name","DB_USERNAME":"Database User",` +
		`"ES_ENDPOINT":"ES URL","REDIS_ENDPOINT":"Redis Endpoint"}`
	return []byte(config)
}

func TestGetConfig(t *testing.T) {
	assertion := assert.New(t)
	configSetting := NewConfigRepository(newTestConfigFile())
	assertion.Equal("DB Host", configSetting.GetConfig("DB_HOST"))
	assertion.Equal("Port of DB", configSetting.GetConfig("DB_PORT"))
	assertion.Equal("Database Name", configSetting.GetConfig("DB_NAME"))
	assertion.Equal("Database User", configSetting.GetConfig("DB_USERNAME"))
	assertion.Equal("ES URL", configSetting.GetConfig("ES_ENDPOINT"))
	assertion.Equal("Redis Endpoint", configSetting.GetConfig("REDIS_ENDPOINT"))
}

func TestFailOnError(t *testing.T) {
	assert.Panics(t, func() { failOnError(errors.New("new error"), "New Error") })
}
